<?php
$digital_ckeck=$this->db->get_where('category',array('category_id'=>$category))->row()->digital;
?>

<section class="page-section">
    <div class="container">
    <h2><?php echo $this->crud_model->get_type_name_by_id('category',$category,'category_name'); ?></h2>
        <div class="row home_category_theme_1" style="border-top: 2px solid <?php echo $color_back;?>;">
        	
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="tabs-wrapper content-tabs">
                    <div class="tab-content">
                        <div class="tab-pane fade in active">
                            <div class="row">
                            	<div class="col-md-3 col-sm-6 col-xs-6 category">
                                	<div class="p-item p-item-type-zoom">
                                        <span class="p-item-hover">
                                            <div class="p-item-info">
                                                <div class="p-headline">
                                                    <span><?php echo $this->crud_model->get_type_name_by_id('category',$category,'category_name'); ?></span>
                                                    <div class="p-line"></div>
                                                    <div class="p-btn">
                                                        <a href="<?php echo base_url(); ?>home/category/<?php echo $category; ?>" class="btn  btn-theme-transparent btn-theme-xs"><?php echo translate('browse'); ?></a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="p-mask"></div>
                                        </span>
                                        <div class="p-item-img">
                                        	<?php 
												if(file_exists('uploads/category_image/'.$this->crud_model->get_type_name_by_id('category',$category,'banner'))){
											?>
                                            <img class="img-responsive image_delay" src="<?php echo img_loading(); ?>" data-src="<?php echo base_url();?>uploads/category_image/<?php echo $this->crud_model->get_type_name_by_id('category',$category,'banner'); ?>" alt=""/>
                                            <?php }else{?>
                                            <img class="img-responsive img-box image_delay" src="<?php echo img_loading(); ?>" data-src="<?php echo base_url();?>uploads/category_image/default.jpg" alt=""/>
                                            <?php }?>
                                        </div>
                                    </div>
                                </div>
                                <?php
                                if(!empty($sub_category)){
								?>
                                <div class="col-md-9 col-sm-6 col-xs-6">
                                	<div class="row">
                                    	<?php
										foreach($sub_category as $row2){
										?>
                                        <div class="col-md-3 col-sm-3 col-xs-3 sub-category">
                                        	<div class="p-item p-item-type-zoom">
                                                <span class="p-item-hover" target="_blank">
                                                    <div class="p-item-info">
                                                        <div class="p-headline">
                                                        	<span><?php echo $this->crud_model->get_type_name_by_id('sub_category',$row2,'sub_category_name'); ?></span>
                                                            <div class="p-line"></div>
                                                            <div class="p-btn">
                                                            	<a href="<?php echo base_url(); ?>home/category/<?php echo $category; ?>/<?php echo $row2; ?>-0" class="btn  btn-theme-transparent btn-theme-xs"><?php echo translate('browse'); ?></a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="p-mask"></div>
                                                </span>
                                                <div class="p-item-img">
                                                	<?php 
														if(file_exists('uploads/sub_category_image/'.$this->crud_model->get_type_name_by_id('sub_category',$row2,'banner'))){
													?>
                                                	<img class="img-responsive img-box image_delay" src="<?php echo img_loading(); ?>" data-src="<?php echo base_url();?>uploads/sub_category_image/<?php echo $this->crud_model->get_type_name_by_id('sub_category',$row2,'banner'); ?>" alt=""/>
                                                    <?php }
													else{
													?>
                                                    <img class="img-responsive img-box image_delay" src="<?php echo img_loading(); ?>" data-src="<?php echo base_url();?>uploads/sub_category_image/default.jpg" alt=""/>
													<?php }?>
                                                </div>
                                            </div>
                                        </div>
                                        <?php
										}
										?>
                                    </div>
                                </div>
                                <?php
								}
								?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>